'use strict';

/**
 * Workspaces.js controller
 *
 * @description: A set of functions called "actions" for managing `Workspaces`.
 */

module.exports = {

  /**
   * Retrieve workspaces records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.workspaces.search(ctx.query);
    } else {
      return strapi.services.workspaces.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a workspaces record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.workspaces.fetch(ctx.params);
  },

  /**
   * Count workspaces records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.workspaces.count(ctx.query);
  },

  /**
   * Create a/an workspaces record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.workspaces.add(ctx.request.body);
  },

  /**
   * Update a/an workspaces record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.workspaces.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an workspaces record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.workspaces.remove(ctx.params);
  }
};
